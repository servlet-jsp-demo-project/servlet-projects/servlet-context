package com.cdac.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {

	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		/*
		 * Load JDBC Driver
		 */
		Class.forName("com.mysql.cj.jdbc.Driver");

		/*
		 * Get connection from DriverManager
		 */
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/sh09?useSSL=false", "root", "root");
		
	}

}
